# End-to-end Dialogue system: Sequence-to-Sequence Learning for Task Oriented Dialogue with Dialogue State Representation

## Goal:
- Re-implementation of the system in paper: https://arxiv.org/pdf/1806.04441.pdf
- Extend the model and achieve better performance

## Project Plan

#### 1) Design (WEEK 1)

**Part one** (time: 1h)
    
1) Decide the framework, e.g. TensorFlow/PyTorch 
2) Decide the dataset, e.g. Kveret. And the partition for train, dev, test sets. 
3) Decide where to run the training, e.g. local GPU, cloud, etc.

**Part Two** (time: 4h)

4) Design the architecture of the network for baseline (according to paper), for each component. This includes the idea of how to translate the formula in the paper into code in the framework we have chosen. We can divide the tasks as follows:
    
    * Encoder & Dialogue State Representation
    
    * Self attention KB & KB Representation
    
    * Decoder: Attention & copy mechanism
    
    * The whole seq2seq, put it altogether
    
    * Training components: loss function (e.g. NLL + RL), evaluation metrics (e.g. BLEU, F1-score)
    
    * Data representation based on the decided architecture on the previous points
    
    * Design Experiment (e.g. following hyperparameter setup like in the paper), perpare toy dataset

**Part Three** (time: 2hr)

5) Think of the idea of possible extension of the model to reach our second goal



#### 2) Implementation of what have designed in point 1.4. (WEEK 2-3)
    
* Preparing the data (time: 2h)

* Implementation of the network (time: 8h)

    * Encoder & DSR

    * Self Attention

    * Decoder

    * The whole seq2seq

    * Training

* Implementation of the experiment (time: 2h)

* Testing: try the implementation on toy dataset (time: 4h)

#### 3) Experiments. Goal: to reach similar result as in the paper (time:8h) (WEEK 4)
    
* Expect time consumption for runnning the experiments (training) and debugging to reach the benchmark


#### 4) Write report (time:6h) (WEEK 5)

#### 5) Re-do step 1-4 for the extended model. (WEEK 5-9)
    
* Notes: It takes more time on designing and running the experiments 
    
    * Rough time estimate: design: 4h, implementation 8h, experiments 16h
    